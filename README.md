# Ansible | Provisionner une instance ec2 à l'aide d'Ansible

_______

**Prénom** : Carlin

**Nom** : FONGANG

**Email** : fongangcarlin@gmail.com

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Nous souhaitons préparer un environnement de déploiement, rédiger le playbook nécessaire et valider son exécution, garantissant ainsi que le serveur web Apache fonctionne correctement et soit accessible de l'extérieur sur le port 80. Ce laboratoire nous permettra d'automatiser la tâche de déploiement du serveur Apache avec Ansible, tout en améliorant l'efficacité et la fiabilité des déploiements.

## Objectifs

Dans ce lab, nous allons :

- Créer une instance : une hôte hébergeant Ansible
- Mettre en place un playbook ansible qui permettra de provisionner une instance ec2 de type t3.medium
- Lancer la configuration sur l'instance cible afin de préparer l'environnement au déploiement d'un conteneur docker
- Pull l'image docker d'Apache (httpd) et le lancer sur la machine cible
- Vérifier enfin que le site par défaut du server Apache est bien disponible.


## Prérequis
Disposer d'un machines avec Ubuntu déjà installées.

Dans notre cas, nous allons provisionner des instances EC2 s'exécutant sous Ubuntu via AWS, sur lesquelles nous effectuerons toutes nos configurations.

Documentation complémentaire :

[Documentation Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)


## Arboréscence du projet

![alt text](img/image-10.png)
*Arborescence du projet*

## 1. Configuration du projet "ansible"
Afin d'avoir une machine ansible toute prête (avec ansible installé), nous allons effectuer un provisionnement grace à terraform en : 

1. Récupérant le dossier [**tf_ec2-ansible_up**](https://gitlab.com/CarlinFongang-Labs/Ansible/lab10-deploy-ec2/-/tree/main/tf_ec2-ansible_up)

2. Ensuite, en suivant les instructions du Readme [Deploiement d'une instance ec2 avec terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/terraform-project#43-description-de-liac-pour-lenvironnement-de-production)


### 1.0. Définition du group_vars/all.yml

````bash
#ec2 information
instance_type: t3.medium
security_group_name: "acd-sg"
id_token: "550e8400-e29b-41d4-a716-446655440079"
host_group_name: "server"
ami_ubuntu22: "ami-0e21465cede02fd1e"
key_name: "devops-aCD"
region: "us-east-1"
host_group: "server"
subnet: "subnet-0e6c19300a7ac97b2"
#ansible_user: "ubuntu"
````

ce fichier spécifie le type d'instance (t3.medium), le nom du groupe de sécurité, le nom du groupe d'hôtes pour l'inventaire Ansible, l'AMI d'Ubuntu 22 à utiliser, la clé SSH, la région AWS (us-east-1), et le sous-réseau pour l'instance.

certaine variable sont spécifiques à la région, tels que l'AMI qu'il retrouver et le subnet qu'il faudra également identifier via la console AWS > VPC > Sous réseaux 


### 1.1. Mise à jour des rôles "requierement"
````bash
---
roles:
  - name: geerlingguy.pip 
  - name: geerlingguy.docker
collections:
  - name: community.aws
````

geerlingguy.pip : S'occupe de l'installation et de la configuration de pip.
geerlingguy.docker : S'occupe de l'installation et de la configuration de Docker.

ces deux rôles sont adéquate et reduirons la taille du playbook à rédiger, en nous évitant de décrire nous même l'installation de pip et docker.

1. Obtenir une ami via la console aws
Console AWS > EC2 > Images > Catalogue des AMI

>![alt text](img/image-8.png)
*identifiant d'une ami aws *

2. Obtenir l'id d'un sous réseau via la console aws

Console AWS > VPC > Cloud privé virtuel > Sous-réseaux

>![alt text](img/image-9.png)
*sous réseau dans un vpc*

### 1.2. Mise à jour de playbook "deploy.yml"

Le playbook **deploy.yml** est divisé en trois parties principales et a pour objectif de déployer un serveur de production sur AWS, puis d'installer et configurer Apache à l'aide de Docker sur ce serveur.

1. Préparation et configuration initiale : **- name: Set localhost**

````bash
---
- name: Set localhost
  hosts: localhost
  become: true
  vars:
    ansible_connection: local
    ansible_python_interpreter: /usr/bin/python3
  pre_tasks:
    - name: Update apt cache
      ansible.builtin.apt:
        update_cache: yes
    - name: Install Python3 and pip
      # Using the `ansible.builtin.apt` module for consistency
      ansible.builtin.apt:
        name:
          - python3
          #- python3-pip
        state: present
        update_cache: yes

    - name: Install pip for Python3
      ansible.builtin.command: apt-get install -y python3-pip
      ignore_errors: yes

    - name: Upgrade pip
      ansible.builtin.command: pip3 install --upgrade pip

    - name: Install AWS boto libraries
      # Adjusted to use `ansible.builtin.pip` for clarity
      ansible.builtin.pip:
        name: "{{ item }}"
      loop:
        - boto
        - boto3
        - botocore
````

Mise à jour du cache apt et installation des prérequis de Python3 et pip sur l'hôte local.
Mise à niveau de pip à sa dernière version.
Installation des bibliothèques AWS boto pour interagir avec les services AWS.

2. Processus de création du serveur : **- name: Server Creation Process**


````bash
- name: Server Creation Process
  hosts: localhost
  tasks:
    - name: Create a security group for server
      # Correct module name from community.aws collection
      ec2_group:
        name: "{{ security_group_name }}"
        description: "{{ security_group_name }}"
        region: "{{ region }}"
        rules:
          - proto: tcp
            from_port: 22
            to_port: 22
            cidr_ip: 0.0.0.0/0
          - proto: tcp
            from_port: 80
            to_port: 80
            cidr_ip: 0.0.0.0/0
        rules_egress:
          - proto: all
            cidr_ip: 0.0.0.0/0

    - name: Deploy Production Server
      # Using the correct module from the community.aws collection
      community.aws.ec2_instance:
        key_name: "{{ key_name }}"
        security_group: "{{ security_group_name }}"
        instance_type: "{{ instance_type }}"
        image_id: "{{ ami_ubuntu22 }}"
        wait: yes
        wait_timeout: 600
        network:
          assign_public_ip: yes
          vpc_subnet_id: "{{ subnet }}"
        tags:
          Name: "ansible-deployed-instance"
        region: "{{ region }}"
      register: ec2

    - name: Get public IP address of the production server
      set_fact:
        public_ip: "{{ ec2.instances | map(attribute='public_ip_address') | list }}"

    - name: Add the newly created host to ec2 group
      add_host:
        hostname: "{{ item }}"
        groups: "{{ host_group_name }}"
      loop: "{{ public_ip }}"

    - name: Add tag to instance
      ec2_tag:
        resource: "{{ item.instance_id }}"
        region: "{{ region }}"
        state: present
        tags:
          Name: "ansible test"
      loop: "{{ ec2.instances }}"

    - name: Wait for SSH to come up on the server
      ansible.builtin.wait_for:
        host: "{{ item.public_ip_address }}"
        port: 22
        state: started
      loop: "{{ ec2.instances }}"
````

Création d'un groupe de sécurité avec des règles spécifiques pour les ports TCP 22 et 80.
Déploiement d'une instance de serveur EC2 sur AWS avec des configurations spécifiées, comme le type d'instance, l'image AMI, le subnet, et l'attribution d'un IP public.
Enregistrement de l'adresse IP publique de l'instance déployée et ajout de cette instance à un groupe d'hôtes pour de futures configurations.
Ajout d'une balise à l'instance et attente de la disponibilité du SSH sur le serveur.

3. Installation d'Apache via Docker : **- name: Apache installation using docker**

````bash
- name: Apache installation using docker
  hosts: "{{ host_group_name }}"
  become: true
  vars_files:
    - group_vars/all.yml

  pre_tasks:
    # Preparing the system for Docker installation
    - name: Update apt cache
      ansible.builtin.apt:
        update_cache: yes

    - name: Install Docker prerequisites
      ansible.builtin.apt:
        name:
          - apt-transport-https
          - ca-certificates
          - curl
          - gnupg-agent
          - software-properties-common
        state: present

    - name: Install Python3 and pip
      # Using the `ansible.builtin.apt` module for consistency
      ansible.builtin.apt:
        name:
          - python3
          #- python3-pip
        state: present
        update_cache: yes

    - name: Install pip for Python3
      ansible.builtin.command: apt-get install -y python3-pip
      ignore_errors: yes

    - name: Upgrade pip
      ansible.builtin.command: pip3 install --upgrade pip

    - name: Download Docker installation script
      ansible.builtin.get_url:
        url: https://get.docker.com
        dest: /tmp/install-docker.sh

    - name: Run Docker installation script
      ansible.builtin.shell: sudo sh /tmp/install-docker.sh

    - name: Install python3-pip
      ansible.builtin.command: python3 -m pip install --upgrade pip

    - name: Install docker python module
      ansible.builtin.pip:
        name: docker

  tasks:
    - name: Create Apache container
      # Using the `community.docker.docker_container` module
      community.docker.docker_container:
        name: webapp
        image: httpd
        ports:
          - "80:80"
````

Préparation du système pour l'installation de Docker, y compris la mise à jour du cache apt et l'installation des prérequis.
Téléchargement et exécution d'un script d'installation de Docker.
Installation du module Docker Python.
Création et déploiement d'un conteneur Docker utilisant l'image Apache HTTP Server (httpd) et configuration du port 80 pour l'accès à l'application web.

### 1.3. Mise à jour du fichier "ansible.cfg"

````bash
decrire de la même façon le fichier ansible.cfg

[defaults]
host_key_checking = False
remote_user = ubuntu
private_key_file = files/.secrets/devops-aCD.pem
ansible_python_interpreter=/usr/bin/python3
````
Le fichier ansible.cfg configure Ansible pour :

- Ignorer la vérification de la clé de l'hôte SSH.
- Utiliser ubuntu comme utilisateur distant par défaut.
- Se connecter en utilisant une clé privée spécifique.
- Exécuter des modules avec Python 3.


### 1.4. Configuration des secrets de sécurité
On va exporter la key ID et la secret key afin de permettre à ansible d'exploiter les api aws pour provisionner de nouvelles ressources.

Dans le terminal on entre : 

````bash
export AWS_ACCESS_KEY_ID="AKIA4MTWIALYZFYB3KAZ"
export AWS_SECRET_ACCESS_KEY="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"
````

en entrant la command `env` on peut afficher les variables d'environnements

>![alt text](img/image.png)
*la paire de clé est bien exportée*

## 1.5. Installation des rôles définit dans le fichier requirements.yml

````bash
ansible-galaxy install -r roles/requirements.yml #ansible-galaxy collection install -r roles/requirements --force
````

l'option **--force** est utilisé ici pour s'assurer qu'en cas de présence d'une précédente configiration de role, que les nouveaux rôles soient appliqués.

>![alt text](img/image-1.png)
*Extraction du rôle réussi*

## 1.6. Exportation de la clé ssh
nous devrons crée un clé ssh qui sera utilisée lors de la connexion de la machine **ansible** à l'instance **cible** qui sera provisionnée, cette clé peut être généré via la console aws, pour cela, dépuis la barre de recherche AWS, saisir EC2 et se rendre dans le service EC2 > Paire de clés > générer une paire de clé

>![alt text](img/image-2.png)
*Créer un paire de clé ssh*

une fois la clé ssh crée, on devra copier cele ci vers notre dossier <project>files/.secret sur notre instance ansible

## 1.7. Deploiement du playbook

pour exécuter le playbook et lancer le provisionnement d'une nouvelle ec2 ainsi que la configuration d'apache, nous allons excuter la commande suivant :

````bash
ansible-playbook deploy.yml --ask-become-pass -vvv

ou 

ansible-playbook deploy.yml -K -vvv
````

>![alt text](img/image-3.png)
*Lancement du playbook*

>![alt text](img/image-4.png)
*Fin du provisionnement et de la configuration avec Ansible*


## 1.8. Vérification du résultat

1. Dépuis la console AWS

L'on peut voir depuis la console AWS, que l'instance à bien été provisionnée et correspond à l'IP que l'on a obtenu en sortie du terminale plus haut

>![alt text](img/image-5.png)
*Instance ec2 provisionnée à l'aide d'Ansible*

2. Connexion à l'instance et vérifiaction des configuration

Dans le playbook, nous avons effectué l'installation de **docker**, ainsi le deploiement d'un conteneur apache

````bash
ssh -i files/.secrets/devops-aCD.pem ubuntu@34.200.238.243
````
>![alt text](img/image-6.png)
*Le conteneur webapp avec pour image httpd est bien déployé*

3. Vérification via le navigateur 

L'on va entrer l'ip de l'instance dans un navigateur pour vérifier que la page web d'apache est bien accéssible

>![alt text](img/image-7.png)
*Pache web d'Apache*


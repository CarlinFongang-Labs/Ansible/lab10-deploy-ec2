---
- name: Deploy
  hosts: localhost
  become: true
  vars:
    ansible_connection: local
    ansible_python_interpreter: /usr/bin/python3
  pre_tasks:
    - name: Update apt cache
      ansible.builtin.apt:
        update_cache: yes
    - name: Install Python3 and pip
      # Using the `ansible.builtin.apt` module for consistency
      ansible.builtin.apt:
        name:
          - python3
          #- python3-pip
        state: present
        update_cache: yes

    - name: Install pip for Python3
      ansible.builtin.command: apt-get install -y python3-pip
      ignore_errors: yes

    - name: Upgrade pip
      ansible.builtin.command: pip3 install --upgrade pip

    - name: Install AWS boto libraries
      # Adjusted to use `ansible.builtin.pip` for clarity
      ansible.builtin.pip:
        name: "{{ item }}"
      loop:
        - boto
        - boto3
        - botocore

- name: Server Creation Process
  hosts: localhost
  tasks:
    - name: Create a security group for server
      # Correct module name from community.aws collection
      ec2_group:
        name: "{{ security_group_name }}"
        description: "{{ security_group_name }}"
        region: "{{ region }}"
        rules:
          - proto: tcp
            from_port: 22
            to_port: 22
            cidr_ip: 0.0.0.0/0
          - proto: tcp
            from_port: 80
            to_port: 80
            cidr_ip: 0.0.0.0/0
        rules_egress:
          - proto: all
            cidr_ip: 0.0.0.0/0

    - name: Deploy Production Server
      # Using the correct module from the community.aws collection
      community.aws.ec2_instance:
        key_name: "{{ key_name }}"
        security_group: "{{ security_group_name }}"
        instance_type: "{{ instance_type }}"
        image_id: "{{ ami_ubuntu22 }}"
        wait: yes
        wait_timeout: 600
        network:
          assign_public_ip: yes
          vpc_subnet_id: "{{ subnet }}"
        tags:
          Name: "ansible-deployed-instance"
        region: "{{ region }}"
      register: ec2

    - name: Get public IP address of the production server
      set_fact:
        public_ip: "{{ ec2.instances | map(attribute='public_ip_address') | list }}"

    - name: Add the newly created host to ec2 group
      add_host:
        hostname: "{{ item }}"
        groups: "{{ host_group_name }}"
      loop: "{{ public_ip }}"

    - name: Add tag to instance
      ec2_tag:
        resource: "{{ item.instance_id }}"
        region: "{{ region }}"
        state: present
        tags:
          Name: "ansible test"
      loop: "{{ ec2.instances }}"

    - name: Wait for SSH to come up on the server
      ansible.builtin.wait_for:
        host: "{{ item.public_ip_address }}"
        port: 22
        state: started
      loop: "{{ ec2.instances }}"

- name: Apache installation using docker
  hosts: "{{ host_group_name }}"
  become: true
  vars_files:
    - group_vars/all.yml

  pre_tasks:
    # Preparing the system for Docker installation
    - name: Update apt cache
      ansible.builtin.apt:
        update_cache: yes

    - name: Install Docker prerequisites
      ansible.builtin.apt:
        name:
          - apt-transport-https
          - ca-certificates
          - curl
          - gnupg-agent
          - software-properties-common
        state: present

    - name: Install Python3 and pip
      # Using the `ansible.builtin.apt` module for consistency
      ansible.builtin.apt:
        name:
          - python3
          #- python3-pip
        state: present
        update_cache: yes

    - name: Install pip for Python3
      ansible.builtin.command: apt-get install -y python3-pip
      ignore_errors: yes

    - name: Upgrade pip
      ansible.builtin.command: pip3 install --upgrade pip

    - name: Download Docker installation script
      ansible.builtin.get_url:
        url: https://get.docker.com
        dest: /tmp/install-docker.sh

    - name: Run Docker installation script
      ansible.builtin.shell: sudo sh /tmp/install-docker.sh

    - name: Install python3-pip
      ansible.builtin.command: python3 -m pip install --upgrade pip

    - name: Install docker python module
      ansible.builtin.pip:
        name: docker

  tasks:
    - name: Create Apache container
      # Using the `community.docker.docker_container` module
      community.docker.docker_container:
        name: webapp
        image: httpd
        ports:
          - "80:80"